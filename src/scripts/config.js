$(document).ready(function () {
  $(".uploadButton").click(function () {
    $(".periodPart").addClass("disabled");
    $(".uploadPart").removeClass("disabled");
    $("body").removeClass("periodActive");
    $("body").addClass("uploadActive");
  });

  $(".periodButton").click(function () {
    $(".uploadPart").addClass("disabled");
    $("body").addClass("periodActive");
    $("body").removeClass("uploadActive");
    $(".periodPart").removeClass("disabled");
  });

  $(".bigInput").change(function () {
    var duration = 10000; // Tempo total da animação em milissegundos (10 segundos)
    var $fill = $(".fill");
    var $h4 = $fill.next("h4");

    // Iniciar animação do texto para branco
    $h4.css("color", "#fff");
    $h4.css("background-image", "url(../assets/config/download-brown.svg)");

    $fill.animate(
      { width: "100%" }, // Aumentar a largura gradualmente até 100%
      {
        duration: duration,
        progress: function (animation, progress) {
          // Definir a cor como verde quando a animação chegar ao fim
          if (progress === 1) {
            $fill.addClass("green");
            $h4.text(
              "O upload do arquivo foi finalizado com sucesso e já está no nosso sistema!"
            );
          }
        },
        complete: function () {
          // Função de callback opcional, executada quando a animação é concluída
          console.log("Animação concluída!");
          $("body").addClass("loading");
          $h4.css("background-image", "url(../assets/config/done.svg)");
        },
      }
    );
  });
});
