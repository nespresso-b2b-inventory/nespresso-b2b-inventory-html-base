$(document).ready(function () {
  $(".tipOpening").click(function () {
    $(this).closest(".containerTip").find(".machineTip").addClass("active");
  });

  $(".closeTip").click(function () {
    $(this).closest(".containerTip").find(".machineTip").removeClass("active");
  });

  $(".selectQuestion").SumoSelect({
    search: false,
    placeholder: "Selecione aqui",
    floatWidth: 0,
  });
  $(".selectQuestion").SumoSelect();

  $(".selectCity").SumoSelect({
    search: false,
    placeholder: "Selecione aqui",
    floatWidth: 0,
  });
  $(".selectCity").SumoSelect();

  $(".selectState").SumoSelect({
    search: false,
    placeholder: "Selecione aqui",
    floatWidth: 0,
  });
  $(".selectState").SumoSelect();
  $(".checkTerms").on("change", function () {
    var isChecked = $(this).prop("checked");
    $(".sendNew").toggleClass("active", isChecked);
  });

  $(document).on("change", ".selectQuestion", function () {
    var selectedValue = $(this).val();
    if (selectedValue === "outro") {
      $(this).closest(".cardInfo").find(".modalText").removeClass("disabled");
    } else {
      $(this).closest(".cardInfo").find(".modalText").addClass("disabled");
      $(this).closest(".cardInfo").find(".modalSummary").addClass("disabled");
    }
  });

  $("textarea").on("input", function () {
    var textareaValue = $(this).val().trim();
    if (textareaValue !== "") {
      $(this).closest(".cardInfo").find(".confirm").addClass("active");
    } else {
      $(this).closest(".cardInfo").find(".confirm").removeClass("active");
      $(".confirm").removeClass("disabled");
      $(".modalSummary").addClass("disabled");
      $(".edit").addClass("disabled");
      $(".confirm").removeClass("disabled");
    }
  });

  $(".addWrapper").click(function () {
    $(this).closest(".addMachine").find(".addContent").removeClass("disabled");
    $(this)
      .closest(".addMachine")
      .find(".closeAddMachine")
      .removeClass("disabled");
  });

  $(".closeAddMachine").click(function () {
    $(this).closest(".addMachine").find(".addContent").addClass("disabled");
    $(this).addClass("disabled");
  });

  // ACTIVE AND FUNCION ONCLICK

  $(".confirm").click(function () {
    if ($(this).hasClass("active")) {
      $(this).closest(".cardInfo").find(".confirm").addClass("disabled");
      $(this).closest(".cardInfo").find(".modalText").addClass("disabled");
      $(this).closest(".cardInfo").find(".edit").removeClass("disabled");
      $(this)
        .closest(".cardInfo")
        .find(".modalSummary")
        .removeClass("disabled");
    } else {
      $(this).closest(".cardInfo").find(".edit").addClass("disabled");
      $(this).closest(".cardInfo").find(".confirm").removeClass("disabled");
    }
  });

  $(".edit").click(function () {
    $(this).closest(".cardInfo").find(".confirm").removeClass("disabled");
    $(this).addClass("disabled");
    $(this).closest(".cardInfo").find(".modalSummary").addClass("disabled");
    $(this).closest(".cardInfo").find(".modalText").removeClass("active");
    $(this).closest(".cardInfo").find(".modalText").removeClass("disabled");
    $(this).closest(".cardInfo").find(".edit").addClass("disabled");
    $(this).closest(".cardInfo").find(".confirm").removeClass("disabled");
    if ($(this).closest(".cardInfo").hasClass("confirmedCard")) {
      $(this)
        .closest(".cardInfo")
        .find(".problem-describe")
        .removeClass("disabled");
      $(this).closest(".cardInfo").find(".modalText").addClass("disabled");
    }
    if ($(this).closest(".cardInfo").hasClass("cardReady")) {
      $(this).closest(".cardInfo").find(".addConfirm").removeClass("disabled");
      $(this).closest(".cardInfo").find(".modalText").addClass("disabled");
      $(this).closest(".cardInfo").find(".informedSerial").addClass("disabled");
      $(this)
        .closest(".cardInfo")
        .find(".selectQuestionWrapper")
        .removeClass("disabled");
      $(this).closest(".cardInfo").find(".marginReady").addClass("no-margin");
    }
  });

  // INPUT PASS

  document.querySelectorAll(".cardInfo").forEach((card) => {
    const inputs = card.querySelectorAll(".input1, .input2, .input3, .input4");
    inputs.forEach((input, index) => {
      input.addEventListener("input", function () {
        if (this.value.length === this.maxLength) {
          // Se o input atingir o tamanho máximo e não for o último, passar o foco para o próximo input
          if (index < inputs.length - 1) {
            inputs[index + 1].focus();
          }

          // Verifica se é o último input (input4) do card e remove o foco
          if (index === inputs.length - 1) {
            this.blur();
          }
        } else if (this.value.length > this.maxLength) {
          // Se o valor digitado for maior que o tamanho máximo, ajustar o valor para o tamanho máximo
          this.value = this.value.slice(0, this.maxLength);
        }
      });
    });
  });

  // ENVIO DE ARQUIVO

  document.querySelectorAll(".cardInfo").forEach((card) => {
    const pictureDiv = card.querySelector(".picture");
    const input = pictureDiv.querySelector("input[type='file']");
    const uploadText = pictureDiv.querySelector("p");
    const selectTag = pictureDiv.querySelector(".selectTag");
    const removeTag = pictureDiv.querySelector(".removeTag");
    const serialNumber = card.querySelector(".serial-number");

    input.addEventListener("change", function (event) {
      const selectedFile = event.target.files[0];
      if (selectedFile) {
        serialNumber.classList.add("allDone");
        selectTag.classList.add("disabled");
        removeTag.classList.remove("disabled");
        uploadText.textContent = selectedFile.name;
      } else {
        serialNumber.classList.remove("allDone");
        selectTag.classList.remove("disabled");
        removeTag.classList.add("disabled");
        uploadText.textContent = "Upload de imagem";
      }
    });
  });

  // ENVIO DE ARQUIVO FORM

  document.querySelectorAll(".addMachine").forEach((card) => {
    const pictureDiv = card.querySelector(".picture");
    const input = pictureDiv.querySelector("input[type='file']");
    const uploadText = pictureDiv.querySelector("p");
    const selectTag = pictureDiv.querySelector(".selectTag");
    const removeTag = pictureDiv.querySelector(".removeTag");
    const contentNumber = card.querySelector(".addContent");

    input.addEventListener("change", function (event) {
      const selectedFile = event.target.files[0];
      if (selectedFile) {
        contentNumber.classList.add("allDone");
        selectTag.classList.add("disabled");
        removeTag.classList.remove("disabled");
        uploadText.textContent = selectedFile.name;
      } else {
        contentNumber.classList.remove("allDone");
        selectTag.classList.remove("disabled");
        removeTag.classList.add("disabled");
        uploadText.textContent = "Upload de imagem";
      }
    });
  });
});
