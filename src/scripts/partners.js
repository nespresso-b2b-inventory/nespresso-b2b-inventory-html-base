const nextBtn =
  '<img src="../assets/partners/next-detail.svg" alt:"seta direita">';
const prevBtn =
  '<img src="../assets/partners/prev-detail.svg" alt:"seta esquerda">';

$(document).ready(function () {
  $(".late").click(function () {
    $(".late").toggleClass("active");
  });

  $(".today").click(function () {
    $(".today").toggleClass("active");
  });

  $(".waiting").click(function () {
    $(".waiting").toggleClass("active");
  });

  $(".next").click(function () {
    $(".next").toggleClass("active");
  });

  $(".checkLabel")
    .closest(".content")
    .click(function (event) {
      if (!$(event.target).is("input") && !$(event.target).is("span")) {
        $("body").addClass("modalDetailActive");
        $(this).closest(".page-item").find(".detail").addClass("active");
      } else {
        console.log;
        if (target.is("label") && target.text().trim() === "Nome") {
          // Adiciona o atributo "checked" a todos os inputs checkbox da página
          $('input[type="checkbox"]').prop("checked", true);
        }
      }
    });

  $(".close").click(function () {
    $("body").removeClass("modalDetailActive");
    $(this).closest(".page-item").find(".detail").removeClass("active");
  });

  $(".modalBackground").click(function (event) {
    if (!$(event.target).closest(".detail").length) {
      $("body").removeClass("modalDetailActive");
      $(".detail").removeClass("active");
    }
  });

  // PAGES

  const itemsPerPage = 20; // Número de itens por página
  const itemList = $("#item-list .page-item");
  const numItems = itemList.length;
  const totalPages = Math.ceil(numItems / itemsPerPage);

  // Verifica se a página inicial é um número inteiro válido e ajusta se necessário
  let startPage = 1; // Defina o número da página inicial aqui
  if (startPage < 1) {
    startPage = 1;
  } else if (startPage > totalPages) {
    startPage = totalPages;
  }

  // Função para personalizar a exibição dos números de página
  function pageDisplayedCallback(event, page) {
    return [page];
  }

  // Configuração da paginação
  $("#pagination-container").twbsPagination({
    totalPages: totalPages,
    startPage: startPage,
    first: '<img src="../assets/partners/begining.svg" alt="Início">',
    prev: '<img src="../assets/partners/prev.svg" alt="Anterior">',
    next: '<img src="../assets/partners/next.svg" alt="Próxima">',
    last: '<img src="../assets/partners/last.svg" alt="Última">',
    onPageClick: function (event, page) {
      const startItem = (page - 1) * itemsPerPage;
      const endItem = startItem + itemsPerPage;
      itemList.hide().slice(startItem, endItem).show();

      // Exibindo o texto personalizado
      const totalResults = numItems;
      const startResult = startItem + 1;
      const endResult = Math.min(startItem + itemsPerPage, totalResults);
      $(".finalResult").text(
        `Exibindo ${startResult}-${endResult} resultados de ${totalResults}`
      );
    },
    pageDisplayed: pageDisplayedCallback, // Chamada da função personalizada
  });

  // CAROUSEL

  $(".carouselDate").owlCarousel({
    navSpeed: 500,
    loop: false,
    center: false,
    margin: 20,
    responsiveClass: true,
    nav: true,
    navText: [prevBtn, nextBtn],
    dots: false,
    autoWidth: false,
    onInitialized: syncListItems,
    onTranslated: syncListItems,
    responsive: {
      0: {
        items: 1,
        margin: 10,
      },
    },
  });

  function syncListItems(event) {
    // Obter o ano do item ativo no carrossel
    const activeItem = event.item.index;
    const year = $(".carouselDate .owl-item")
      .eq(activeItem)
      .find(".item p")
      .text(); // Altere para "data-year" se preferir obter o valor do atributo data-year

    // Remover a classe de borda de todos os itens visíveis da lista
    $(".listDetail .containerItem").removeClass("no-border-bottom");

    // Mostrar apenas os itens da lista com o ano correspondente
    $(".listDetail .containerItem").hide();
    $(`.listDetail .containerItem[data-year="${year}"]`).show();

    // Adicionar classe de borda ao último item visível mostrado na lista com o data-year correspondente
    $(`.listDetail .containerItem[data-year="${year}"]:visible`)
      .last()
      .addClass("no-border-bottom");
  }

  // CHECK NAMES

  $(".checkNames").on("click", function () {
    // Verifica se o input checkNames está marcado
    if ($(this).prop("checked")) {
      // Seleciona todos os inputs com a classe "checkTerms" dentro da lista com o id "item-list"
      $("#item-list .checkTerms").prop("checked", true);
    } else {
      // Caso contrário, desmarca todos os inputs com a classe "checkTerms"
      $("#item-list .checkTerms").prop("checked", false);
    }
  });
});
