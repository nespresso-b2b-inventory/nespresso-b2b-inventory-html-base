$(document).ready(function () {
  const itemsPerPage = 20; // Número de itens por página
  const itemList = $("#item-list .item");
  const numItems = itemList.length;
  const totalPages = Math.ceil(numItems / itemsPerPage);

  // Verifica se a página inicial é um número inteiro válido e ajusta se necessário
  let startPage = 1; // Defina o número da página inicial aqui
  if (startPage < 1) {
    startPage = 1;
  } else if (startPage > totalPages) {
    startPage = totalPages;
  }

  // Função para personalizar a exibição dos números de página
  function pageDisplayedCallback(event, page) {
    return [page];
  }

  // Configuração da paginação
  $("#pagination-container").twbsPagination({
    totalPages: totalPages,
    startPage: startPage,
    first: '<img src="../assets/partners/begining.svg" alt="Início">',
    prev: '<img src="../assets/partners/prev.svg" alt="Anterior">',
    next: '<img src="../assets/partners/next.svg" alt="Próxima">',
    last: '<img src="../assets/partners/last.svg" alt="Última">',
    onPageClick: function (event, page) {
      const startItem = (page - 1) * itemsPerPage;
      const endItem = startItem + itemsPerPage;
      itemList.hide().slice(startItem, endItem).show();

      // Exibindo o texto personalizado
      const totalResults = numItems;
      const startResult = startItem + 1;
      const endResult = Math.min(startItem + itemsPerPage, totalResults);
      $(".finalResult").text(
        `Exibindo ${startResult}-${endResult} resultados de ${totalResults}`
      );
    },
    pageDisplayed: pageDisplayedCallback, // Chamada da função personalizada
  });
});
