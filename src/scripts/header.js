$(document).ready(function () {
  var h3Selector = "header.menu .config h3";

  // Adicione um manipulador de evento de clique para o h3
  $(document).on("click", h3Selector, function () {
    $(this).next("nav").toggleClass("active");
    $("body").toggleClass("navOpen");
  });
});
