$(document).ready(function () {
  $(".eye").click(function () {
    $("body").toggleClass("eyesOff");

    const bodyClass = $("body").hasClass("eyesOff");
    const passwordInput = $(".password-eye");

    if (bodyClass) {
      passwordInput.attr("type", "text");
    } else {
      passwordInput.attr("type", "password");
    }
  });

  $(".enter").click(function () {
    $("body").addClass("wrongPassword");
  });
});
