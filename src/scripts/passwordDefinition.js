$(document).ready(function () {
  $(".password-first, .repeat").on("input", function () {
    var passwordFirst = $(".password-first").val();
    var repeatPassword = $(".repeat").val();

    // Adicionar classe 'characters' ao body se a senha tiver 8 letras
    if (passwordFirst.length >= 8) {
      $("body").addClass("characters");
    } else {
      $("body").removeClass("characters");
    }

    // Adicionar classe 'uppercase' ao body se a senha tiver uma letra maiúscula
    if (/[A-Z]/.test(passwordFirst)) {
      $("body").addClass("uppercase");
    } else {
      $("body").removeClass("uppercase");
    }

    // Adicionar classe 'special' ao body se a senha tiver um número ou caractere especial
    if (/\d|[^a-zA-Z0-9]/.test(passwordFirst)) {
      $("body").addClass("special");
    } else {
      $("body").removeClass("special");
    }

    // Adicionar classe 'no-match' ao body se as senhas não forem iguais
    if (passwordFirst !== repeatPassword) {
      $("body").addClass("no-match");
    } else {
      $("body").removeClass("no-match");
    }
  });

  $("#eye1").click(function () {
    $("body").toggleClass("eyesOff1");

    const bodyClass = $("body").hasClass("eyesOff1");
    const passwordInput = $(".inputEye1");

    if (bodyClass) {
      passwordInput.attr("type", "text");
    } else {
      passwordInput.attr("type", "password");
    }
  });

  $("#eye2").click(function () {
    $("body").toggleClass("eyesOff2");

    const bodyClass = $("body").hasClass("eyesOff2");
    const passwordInput = $(".inputEye2");

    if (bodyClass) {
      passwordInput.attr("type", "text");
    } else {
      passwordInput.attr("type", "password");
    }
  });
});
